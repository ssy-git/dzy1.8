import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import qs from 'qs'
import {$get,$postkv,$postf,$postj} from '@/ajax'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import 'swiper/css/swiper.css';
import axios from 'axios';


Vue.config.productionTip = false
// 第三方请求，需要设置代理地址————axios.defaults.baseURL = '/api';
Vue.prototype.$axios = axios;		// 使用时，this.$axios.请求方式('请求地址')
Vue.prototype.$get = $get;
Vue.prototype.$postkv =$postkv;
Vue.prototype.$postj = $postj;
Vue.prototype.$postf =$postf;
Vue.prototype.$qs = qs;
Vue.prototype.$ip = "http://127.0.0.1:80";

Vue.use( ElementUI );
Vue.use( VueAwesomeSwiper );
// Vue.use( axios );

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
