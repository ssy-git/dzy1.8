// 导入vue
import Vue from 'vue';
// 导入vuex
import Vuex from 'vuex';
// 导入持久化存储插件
import createPersistedState from 'vuex-persistedstate';
// 
import search from './moduies/search';
import user from './moduies/user';
import order from './moduies/order';

// 在Vue中应用插件/注册插件
Vue.use( Vuex );

// 配置对象
const store = new Vuex.Store({
	// 状态：存储共享的数据
	state: {
		name:'',
		role:'',
		account:'',
		img:'',
	},
	// 创建同步函数，主要作用：修改state内部的数据
	mutations : {
		set(state,obj){
			state.name=obj.name;
			state.role=obj.role;
			state.account=obj.account;
			state.img=obj.img;
		},
		del(state){
			state.name='';
			state.role='';
			state.account='';
			state.img='';
		}
	},
	// 创建异步函数，主要作用：先被组件调用，然后在函数内部调用mutations的同步函数
	actions : {
		setUser({state,commit},data){
			commit('set',data);
		},
		delHistory({state,commit}){
			commit('del');
		}
	},
	
	// 设置数据：不改变state的数据，return返回另外一个形式 【类似组件computed】
	getters : {},
	// 设置模块开发：指向按照功能将数据分别进行不同的模块
	// 跟注册登录相关的数据划分一个模块；购物车相关的数据一个模块；订单数据一个模块【数据量庞大且操作较多的功能区域】
	modules : {
		// 应用注册的搜索模块
		search,
		order,
		user
	},
	
	// 设置store持久化存储，与本地缓存同步
	plugins : [ createPersistedState() ]
})

// 导出
export default store;