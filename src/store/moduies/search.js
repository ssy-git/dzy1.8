// 主要负责存储历史记录
// 1.创建存储搜索记录的数据状态
const state = {
	hlist : [],		//存储历史记录的数组
	cart : []
}

// 2.同步函数——设置修改state的函数操作
const mutations = {
	// 向数组添加元素
	// state表示共享数据，keyword表示组件传递的数据
	addHistoryList( state , keyword ){
		state.hlist.push( keyword );
	},
	delHistoryList(state){
		state.hlist = [];
	}
}

// 3.异步函数——设置提供给组件调用的异步函数
const actions = {
	// 创建一个函数：被组件调用且获取组件传递的数据
	// state：表示共享数据，但是只能读取
	// commit：表示mutations同步函数对象
	// keyword：表示组件传递的数据
	actionHistory( {state, commit}, keyword ){
		// 调用mutations的函数，将keyword传递过去
		commit('addHistoryList' , keyword);
	},
	delHistory( {state, commit}, msg ){
		commit('delHistoryList', msg);
	}
}
// 如果5大核心在模块上没有应用，可以省略不写
// 导出
export default{
	// 注册当前模块
	namespaced : true,
	state,
	mutations,
	actions,
}