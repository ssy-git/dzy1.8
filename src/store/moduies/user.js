const state = {
	user : null,
	login : [],
	usertype : '',
	Authorization : '',
}

const mutations = {
	saveUser(state,obj){
		state.user = obj;
	},
	delUser(state){
		state.user = null;
	},
	deleteLogin(state){
		state.login = [];
		state.Authorization = '';
	},
	addLogin(state,obj){
		state.login.push(obj);
	}
}

const actions = {
	selWord( {state,commit}, obj){
		commit('saveUser', obj)
	},
	LoginUser( {state,commit}, msg){
		commit('pushLogin', msg)
	},
	DelLogin( {state,commit}, msg){
		commit('deleteLogin', msg)
	}
}

export default{
	namespaced : true,
	state,
	mutations,
	actions
}