//主要负责存储历史记录
// 1.创建存储搜索记录的数据状态
const state = {
	orders:[],
}
// 同步函数——设置修改state的函数操作
const mutations = {
	// 向数组条件元素
	// state 表示共享数据，keyword表示组将传递的数据
	addOrders(state,keyword){
		state.orders.push(keyword);
	},
	delOrders(state){
		state.orders = [];
	}
}
// 3.异步函数——设置提供给组将调用的异步函数
const actions = {
	// 创建一个函数 被组件调用且换取组件传递的数据
	// state 表示共享数据，但是只能读取
	// commit表示mutations同步函数对象
	// keyword 组件传递过来的数据
	actionHistory({state,commit},keyword){
		// 调用mutations的函数，将keyword传递过去
		commit('addOrders',keyword);
	},
	delHistory({state,commit}){
		commit('delOrders');
	}
}

//导出
export default {
	// 注册当前模块
	namespaced :true,
	state,
	mutations,
	actions,
}