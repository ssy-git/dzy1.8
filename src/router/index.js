import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Mobile from '@/view/Mobile.vue';
import MobileHeader from '@/components/mobile/default/MobileHeader.vue';
// app首页
import MobileHome from '@/view/mobile/MobileHome.vue';
// 书库页面
import MobileBooksList from '@/view/mobile/MobileBooksList.vue';
//书本详情
import MobileBookDetail from '@/view/mobile/MobileBookDetail.vue';
// 搜索页
import MobileSearch from '@/view/mobile/MobileSearch.vue';

// 用户页面
import Mine from '@/view/mobile/Mine.vue';
// 登录页面
import MobileLogin from '@/view/mobile/MobileLogin.vue';
// 注册页面
import MobileReg from '@/view/mobile/MobileReg.vue';
// 购物车
import MobileCart from '@/components/mobile/home/MobileCart.vue';
// 提交订单页面
import MobileOrderPay from '@/components/mobile/MobileOrderPay.vue';
// 用户订单
import MobileOrder from '@/components/mobile/home/MobileOrder.vue';
// 用户设置页面
import MobileSet from '@/components/mobile/home/MobileSet.vue';
// 修改密码
import MobilePwd from '@/components/mobile/home/MobilePwd.vue';
// 修改照片
import MobilePic from '@/components/mobile/home/MobilePic.vue';
//修改个人信息
import MobileInfo from '@/components/mobile/home/MobileInfo.vue';
import MobileFooter from '@/components/mobile/default/MobileFooter.vue';

import AdmHome from '@/view/admin/AdmHome.vue';
import AdmPic from '@/components/admin/AdmPic.vue';
import HotPicAdd from '@/components/admin/HotPicAdd.vue';
import AdmType1 from '@/components/admin/AdmType1.vue';
import AdmType2 from '@/components/admin/AdmType2.vue';
import AdmGoods from '@/components/admin/AdmGoods.vue';
import GoodsAdd from '@/components/admin/GoodsAdd.vue';
import AdmOrder from '@/components/admin/AdmOrder.vue';

import PcHome from '@/view/pc/PcHome.vue';
import PcHeader from '@/components/pc/default/PcHeader.vue';
//pc主体
import PcMain from '@/view/pc/PcMain.vue';
// 书本详情
import PcBkInfo from '@/components/pc/home/BookInfo.vue';

// 书本合集页面
import Books from '@/components/pc/book/Books.vue';
import GitBody from '@/components/pc/book/GitBody.vue';

import PcCart from '@/components/pc/home/Cart.vue';
import PcOrder from '@/components/pc/order/Order.vue';
import PcMyOrd from '@/components/pc/order/MyOrd.vue';
import UserReg from '@/components/UserReg.vue';
import UserSet from '@/components/pc/setting/UserSet.vue';
import SetHome from '@/components/pc/setting/SetHome.vue'
import SetNav from '@/components/pc/setting/SetNav.vue';
import PcPwd from '@/components/pc/setting/PcUpdatePwd.vue';
import PcPic from '@/components/pc/setting/PcUpdatePic.vue';
import PcInfo from '@/components/pc/setting/PcUpdateInfo.vue';

import BookGit from '@/components/pc/book/BookGit.vue';
import PcFooter from '@/components/pc/default/PcFooter.vue';


import Login from '@/components/Login.vue';




const routes = [{
		path: '/',
		// redirect : { name : Home }
		redirect: '/login'
	},
	{
		path: '/login',
		name: 'Login',
		component: Login,
	},

	{
		path: '/pc',
		name: 'PcHome',
		component: PcHome,
		children: [{
				path: 'home',
				name: 'PcMain',
				components: {
					header: PcHeader,
					default: PcMain,
					footer: PcFooter
				}
			},
			{
				path: 'bookinfo/:bookid',
				name: 'PcBkInfo',
				components: {
					header: PcHeader,
					default: PcBkInfo,
					footer: PcFooter
				}
			},
			{
				path: 'books',
				name: 'Books',
				components: {
					header: PcHeader,
					default: GitBody,
					footer: PcFooter
				}
			},
			{
				path: 'cart',
				name: 'PcCart',
				components: {
					header: PcHeader,
					default: PcCart,
					footer: PcFooter
				}
			},
			{
				path: 'order',
				name: 'PcOrder',
				components: {
					header: PcHeader,
					default: PcOrder,
					footer: PcFooter
				}
			},
			{
				path: 'myOrd',
				name: 'PcMyOrd',
				components: {
					header: PcHeader,
					default: PcMyOrd,
					footer: PcFooter
				}
			},
			{
				path: 'reg',
				name: 'UserReg',
				components: {
					header: PcHeader,
					default: UserReg,
					footer: PcFooter
				}
			},
			{
				path: 'uSet',
				name: 'UserSet',
				components: {
					header: PcHeader,
					default: UserSet,
					footer: PcFooter
				},
				children: [{
						path: 'sethome',
						name: 'SetHome',
						components: {
							navigation: SetNav,
							default: SetHome
						}
					},
					{
						path: 'updatePwd',
						name: 'PcPwd',
						components: {
							navigation: SetNav,
							default: PcPwd
						}
					},
					{
						path: 'updatePic',
						name: 'PcPic',
						components: {
							navigation: SetNav,
							default: PcPic
						}
					},
					{
						path: 'updateInfo',
						name: 'PcInfo',
						components: {
							navigation: SetNav,
							default: PcInfo
						}
					}
				]
			},
		]
	},

	{
		path: '/adm',
		name: 'AdmHome',
		component: AdmHome,
		children: [{
				path: 'hotpic',
				name: 'AdmPic',
				component: AdmPic,
			},
			{
				path: 'hotpicadd',
				name: 'HotPicAdd',
				component: HotPicAdd
			},
			{
				path: 'type1',
				name: 'AdmType1',
				component: AdmType1
			},
			{
				path: 'type2',
				name: 'AdmType2',
				component: AdmType2
			},
			{
				path: 'goods',
				name: 'AdmGoods',
				component: AdmGoods
			},
			{
				path: 'addGoods',
				name: 'GoodsAdd',
				component: GoodsAdd
			},
			{
				path: 'order',
				name: 'AdmOrder',
				component: AdmOrder
			}
		]
	},


	{
		path: '/mobile',
		name: 'Mobile',
		component: Mobile,
		// 子路由不要携带斜杠！！！有斜杠是渲染失败的
		children: [{
				path: 'home',
				name: 'MobileHome',
				// 下面所使用的是：命名视图的方式，为router-view的name属性匹配组件
				components: {
					default: MobileHome, //渲染到没有name属性的router-view上
					header: MobileHeader,
					footer: MobileFooter
				}
			},
			{
				path: 'bookslist',
				name: 'MobileBooksList',
				components: {
					default: MobileBooksList,
					header: MobileHeader,
					footer: MobileFooter
				}
			},
			{
				path: 'mine',
				name: 'Mine',
				components: {
					default: Mine,
					header: MobileHeader,
					footer: MobileFooter
				}
			},
			{
				path: 'bookdetail/:bookid',
				name: 'MobileBookDetail',
				component: MobileBookDetail
			},
			{
				path: 'search',
				name: 'MobileSearch',
				component: MobileSearch
			},
			{
				path: 'login',
				name: 'MobileLogin',
				component: MobileLogin
			},
			{
				path: 'reg',
				name: 'MobileReg',
				component: MobileReg
			},
			{
				path: 'cart',
				name: 'MobileCart',
				component: MobileCart
			},
			{
				path: 'order',
				name: 'MobileOrder',
				component: MobileOrder
			},
			{
				path: 'orderpay',
				name: 'MobileOrderPay',
				component: MobileOrderPay
			},
			{
				path: 'uSet',
				name: 'MobileSet',
				component: MobileSet
			},
			{
				path: 'updatepwd',
				name: 'MobilePwd',
				component: MobilePwd
			},
			{
				path: 'updatepic',
				name: 'MobilePic',
				component: MobilePic
			},
			{
				path: 'updateinfo',
				name: 'MobileInfo',
				component: MobileInfo
			}
		]
	}
];


const router = new VueRouter({
	routes
})

// 全局路由守卫：会拦截所有的路由跳转行为
// 常用于实现登录拦截
router.beforeEach((to, from, next) => {
	// console.log(to, from)
	// 只对访问购物车组件的路由跳转行为进行拦截
	console.log(to.fullPath);
	if (to.fullPath == '/pc/cart' || to.fullPath == '/pc/myOrder' ||
		to.fullPath == '/mobile/cart' || to.fullPath == '/mobile/order') {
		let token = window.localStorage.getItem('token')
		if (token) {
			next()
		} else {
			Message.error('未登录！请登录');
		}
	} else {
		next()
	}
})

export default router