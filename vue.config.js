module.exports = {
	// 配置开发服务器
	devServer : {
		// 设置反向代理
		proxy : {
			// 设置一个请求的开头，会自动将请求的地址拼接到设置请求开头
			'/api' : {
				// 设置目标服务器地址
				target : 'http://127.0.0.1',
				// 允许跨域
				changeOrigin : true,
				// 设置axios请求的时候，重写请求地址：将默认localhost:8080重写成目标服务器地址
				pathRewrite : {
					// ^：不管是什么，都重写
					'^/api' : '/'
				}
			}
		}
	}
}